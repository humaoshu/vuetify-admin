import Cookies from 'js-cookie';

// User
const tokenKey = 'sinzk_open_admin_access_token';
export const getToken = () => Cookies.get(tokenKey);
export const setToken = (token: string) => Cookies.set(tokenKey, token);
export const removeToken = () => Cookies.remove(tokenKey);

export const setCode = (code: string) =>
  Cookies.set('code', code, { expires: 1 });
export const getCode = () => Cookies.get('code');
export const removeCode = () => Cookies.remove('code');
