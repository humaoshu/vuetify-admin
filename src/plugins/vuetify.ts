import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import zhHans from 'vuetify/src/locale/zh-Hans';
// import '@/sass/main.scss';
import 'vuetify/dist/vuetify.min.css';

Vue.use(Vuetify);

export default new Vuetify({
  lang: {
    locales: { zhHans },
    current: 'zh-Hans',
  },
  icons: {
    iconfont: 'md',
  },

  theme: {
    themes: {
      light: {
        primary: '#9C27B0',
        secondary: '#b0bec5',
        accent: '#8c9eff',
        error: '#F44336',
        info: '#2196F3',
        warning: '#FF9800',
        success: '#4CAF50',
      },
    },
  },
});
