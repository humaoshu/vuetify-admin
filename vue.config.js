const path = require("path");
const devServerPort = 9531; // TODO: get this variable from setting.ts
const mockServerPort = 9530; // TODO: get this variable from setting.ts
module.exports = {
  // 配置less
  publicPath: process.env.NODE_ENV === 'production' ? '/vue-admin/' : '/', // TODO: Remember to change this to fit your need
  lintOnSave:process.env.NODE_ENV==='development',
  // vue.config.js

//   css: {
//     loaderOptions: {
//       sass: {
//         data: `@import "~@/sass/main.scss"`
//       }
//     }
//   },

  // 配置端口号,修改本地端口指向网址，即可解除微信ip限制
  devServer: {
    port: devServerPort,
    open: true,
    overlay: {
      warnings: false,
      errors: true
    },
    proxy: {
      // change xxx-api/login => /mock-api/v1/login
      // detail: https://cli.vuejs.org/config/#devserver-proxy
      [process.env.VUE_APP_BASE_API]: {
        target: `http://localhost:${mockServerPort}/mock-api/v1`,
        changeOrigin: true, // needed for virtual hosted sites
        ws: true, // proxy websockets
        pathRewrite: {
          ["^" + process.env.VUE_APP_BASE_API]: ""
        }
      }
    }
  }
  // devServer: {
  //     disableHostCheck: true,
  //     port: devServerPort
  // }
};
