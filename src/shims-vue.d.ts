declare module '*.vue' {
  import Vue from 'vue';
  export default Vue;
}
declare module 'vuetify/lib' {
  export * from 'vuetify';
}
declare module 'swagger-ui';
declare module 'mini-toastr';
