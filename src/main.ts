import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';
import '@/permission';
import Notification from 'vue-notification';

Vue.config.productionTip = false;

Vue.use(Notification);

export default new Vue({
  router,
  store,
  vuetify,
  render: h => h(App),
}).$mount('#app');
