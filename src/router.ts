import Vue from 'vue';
import Router from 'vue-router';
import Login from '@/views/Login.vue';
import Layout from '@/layouts/DefaultLayout.vue';
import { Grade } from '@/enums/index';

Vue.use(Router);

export default new Router({
  scrollBehavior: (to, from, savedPosition) => {
    if (savedPosition) {
      return savedPosition;
    } else {
      return { x: 0, y: 0 };
    }
  },
  routes: [
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: { title: '登录', icon: 'network_locked' },
    },

    {
      path: '/',
      component: Layout,
      redirect: '/dashboard',
      meta: { title: '首页', icon: 'home_work', grade: Grade.Father },
      children: [
        {
          path: '/dashboard',
          name: 'dashboard',
          meta: {
            title: '首页',
            icon: '#iconHOMEMESSAGE',
            grade: Grade.Children,
          },
          component: () => import('./views/dashboard/index.vue'),
        },
      ],
    },

    {
      path: '/api',
      meta: { title: '接口文档', icon: 'dashboard', grade: Grade.Father },
      component: Layout,

      children: [
        {
          path: '/api/app',
          name: 'api',
          meta: {
            title: '应用管理',
            icon: '#iconApplication-Vote',
            grade: Grade.Children,
          },
          component: () => import('./views/apiDoc/Application.vue'),
        },
        {
          path: '/api/Authentication',
          name: 'authentication',
          meta: {
            title: '身份验证',
            icon: '#iconAuthentication',
            grade: Grade.Children,
          },
          component: () => import('./views/apiDoc/Authentication.vue'),
        },
        {
          path: '/api/Authorization',
          name: 'authorization',
          meta: {
            title: '授权管理',
            icon: '#iconAnoverviewof_icon_authorization',
            grade: Grade.Children,
          },
          component: () => import('./views/apiDoc/Authorization.vue'),
        },
      ],
    },

    {
      path: '/table',
      meta: { title: '业务表单', icon: 'table', grade: Grade.Father },
      component: Layout,

      children: [
        {
          path: '/table/basicTable',
          name: 'basicTable',
          meta: {
            title: '基础表格',
            icon: '#iconApplication-Vote',
            grade: Grade.Children,
          },
          component: () => import('./views/table/BasicTable.vue'),
        },

        {
          path: '/table/DataTable',
          name: 'authorization',
          meta: {
            title: '数据表格',
            icon: '#iconAnoverviewof_icon_authorization',
            grade: Grade.Children,
          },
          component: () => import('./views/table/DataTable.vue'),
        },
      ],
    },
  ],
});
